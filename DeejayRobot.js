module.exports = class DeejayRobot {
    constructor(config) {
        const TelegramBot = require('node-telegram-bot-api')
        const Spotify = require('node-spotify-api')
        const options = config.telegram.mode == 'webhook' ? { webHook: config.telegram.webhook.options } : { polling: true }

        this.bot = new TelegramBot(config.telegram.token, options)
        this.spotify = new Spotify(config.spotify)

        if (config.telegram.mode == 'webhook') {
            this.updateWebhook(config)
        }

        this.bot.on('inline_query', (msg) => {
            try {
                if (msg.query != '' || msg.query != undefined) {
                    if (msg.query.includes('https://open.spotify.com/track/')) {
                        this.getResultsByID(msg.query).then((results) => {
                            this.bot.answerInlineQuery(msg.id, results)
                        })
                    } else {
                        this.getResultsBySearch(msg.query).then((results) => {
                            this.bot.answerInlineQuery(msg.id, results)
                        })
                    }
                }
            } catch (error) {
                console.error(error)
            }
        })
    }

    updateWebhook(config) {
        var options = {}
        if (config.telegram.uploadCertificate != undefined) {
            options = { certificate: config.telegram.webhook.options.cert }
        }
        this.bot.setWebHook(`${config.telegram.webhook.url}/bot${config.telegram.token}`, options)
    }

    getResultsByID(id) {
        var track = id.split('https://open.spotify.com/track/')[1]
        var track_request = this.spotify.request('https://api.spotify.com/v1/tracks/' + track)

        return track_request.then((response) => {
            if (response.preview_url != null) {
                var results = [
                    {
                        type: 'audio',
                        id: 1,
                        audio_url: response.preview_url,
                        title: response.name + ' - ' + response.artists[0].name,
                    }
                ]
                return results
            }
        }).catch((err) => {
            console.error(err)
            return []
        })
    }

    getResultsBySearch(query) {
        return this.spotify.search({ type: 'track', query: query }).then((response) => {
            var results = []
            var index = 0
            response.tracks.items.map((track) => {
                if (track.preview_url != null) {
                    results.push({
                        type: 'audio',
                        id: index,
                        audio_url: track.preview_url,
                        title: track.name + ' - ' + track.artists[0].name,
                    })
                    index++
                }
            })
            return results
        }).catch((err) => {
            console.error(err)
            return []
        })
    }
}