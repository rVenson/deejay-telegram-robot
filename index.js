const config = require('./configure').getConfig()
const DeejayRobot = require('./DeejayRobot')

if (config != null) {
    const app = new DeejayRobot(config)
} else {
    console.error("Não foi encontrado um arquivo de configuração válido")
}