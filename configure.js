const fs = require('fs')
const filePath = './config.json'

module.exports.getConfig = function() {
    const isConfigExists = fs.existsSync(filePath)
    
    if(isConfigExists){
        var config = require(filePath)
        try{
            validateFile(config)
            return config
        } catch(err){
            console.error("Foram verificados erros no arquivo de configuração " + filePath)
            console.error(err)
            return null
        }
    } else {
        console.error("Você deve criar um arquivo de configuração!")
        return null
    }
}

function validateFile(config){
    if(config.telegram.token == '')
    throw new Error("Não foi identificado o TOKEN da Telegram Bot API")

    if(config.telegram.mode != 'polling' && config.telegram.mode != 'webhook')
    throw new Error("Os modos de funcionamento da API Telegram são 'polling' ou 'webhook'")

    if(config.spotify.id == '')
    throw new Error("Não foi identificado o ID da Spotify API")

    if(config.spotify.secret == '')
    throw new Error("Não foi identificado o SECRET da Spotify API")

    if(config.telegram.mode == 'webhook'){
        if(config.telegram.webhook.url == '')
        throw new Error("Não foi identificado a URL da Telegram Bot API")

        if(config.telegram.webhook.options.port == undefined)
        throw new Error("Você deve definir uma porta para o webhook")

        if(config.telegram.webhook.options.key == undefined)
        throw new Error("Você deve especificar o caminho da chave SSL para o webhook")

        if(config.telegram.webhook.options.cert == undefined)
        throw new Error("Você deve especificar o caminho do certificado SSL para o webhook")
    }
}